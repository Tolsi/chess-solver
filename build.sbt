name := "chess-solver"

version := "1.2"

scalaVersion := "2.11.7"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"
libraryDependencies += "com.google.guava" % "guava" % "19.0"
import pl.project13.scala.sbt.JmhPlugin

enablePlugins(JmhPlugin)

javaOptions += "-Xmx4g"
fork in Test := true
fork in run := true
connectInput in run := true
outputStrategy := Some(StdoutOutput)