package ru.tolsi.chess.solvers.benchmark

trait WithRightAnswerCheck {
  def rightAnswer: Int

  protected def assertRightAnswer(calculated: Int) = assert(calculated == rightAnswer, s"$calculated isn't right answer (!= $rightAnswer)")
}
