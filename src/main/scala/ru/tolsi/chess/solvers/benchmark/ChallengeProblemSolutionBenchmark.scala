package ru.tolsi.chess.solvers.benchmark

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.withcache
import ru.tolsi.chess.solvers.dfs.withoutcache

@State(Scope.Benchmark)
class ChallengeProblemSolutionBenchmark extends WithRightAnswerCheck {

  override final val rightAnswer = 3063828

  val problem = new ChessProblem(7, 7, Seq(King, King, Queen, Queen, Bishop, Bishop, Knight))

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithoutCacheSolver(): Unit = {
    val solver = new withoutcache.DFSSolver()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def parallelDfsWithCacheSolver(): Unit = {
    val solver = new withcache.ParallelDFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithCacheSolver(): Unit = {
    val solver = new withcache.DFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithRotationsAndCacheSolver(): Unit = {
    val solver = new withcache.DFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def weakDfsWithRotationsWithCacheSolver(): Unit = {
    val solver = new withcache.WeakDFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }
}
