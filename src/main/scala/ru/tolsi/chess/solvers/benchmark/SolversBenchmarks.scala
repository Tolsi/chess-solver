package ru.tolsi.chess.solvers.benchmark

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.withcache
import ru.tolsi.chess.solvers.dfs.withoutcache
import ru.tolsi.chess.solvers.bruteforce.BruteForceSolver

@State(Scope.Benchmark)
class SolversBenchmarks extends WithRightAnswerCheck {

  val problem = new ChessProblem(5, 5, Seq(Rook, Rook, Knight, Knight, Knight, Knight))
  override final val rightAnswer = 1402

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def bruteForceSolver(): Unit = {
    val solver = new BruteForceSolver(new SafePositionChecker)
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithoutCacheSolver(): Unit = {
    val solver = new withoutcache.DFSSolver()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def parallelDfsWithCacheSolver(): Unit = {
    val solver = new withcache.ParallelDFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def parallelDfsSolver(): Unit = {
    val solver = new withoutcache.ParallelDFSSolver()
    assertRightAnswer(solver(problem).size)
  }


  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithCacheSolver(): Unit = {
    val solver = new withcache.DFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def dfsWithRotationsAndCacheSolver(): Unit = {
    val solver = new withcache.DFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 10, timeUnit = TimeUnit.MINUTES)
  def weakDfsWithRotationsWithCacheSolver(): Unit = {
    val solver = new withcache.WeakDFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }
}
