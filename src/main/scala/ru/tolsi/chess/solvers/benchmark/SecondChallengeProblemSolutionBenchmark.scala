package ru.tolsi.chess.solvers.benchmark

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.{withcache, withoutcache}

@State(Scope.Benchmark)
class SecondChallengeProblemSolutionBenchmark extends WithRightAnswerCheck {

  override final val rightAnswer = 20136752

  val problem = new ChessProblem(6, 9, Seq(King, King, Queen, Rook, Bishop, Knight))

  @Benchmark
  @BenchmarkMode(Array(Mode.AverageTime))
  @Fork(jvmArgs = Array("-Xms4g", "-Xmx4g"))
  @Timeout(time = 30, timeUnit = TimeUnit.MINUTES)
  def weakDfsWithRotationsWithCacheSolver(): Unit = {
    val solver = new withcache.WeakDFSSolverWithRotations()
    assertRightAnswer(solver(problem).size)
  }
}
