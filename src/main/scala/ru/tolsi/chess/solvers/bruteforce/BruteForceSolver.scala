package ru.tolsi.chess.solvers.bruteforce

import ru.tolsi.chess._

class BruteForceSolver(positionChecker: PositionChecker) extends ChessProblemSolver {
  val allPositionGenerator = new AllPositionsGenerator()

  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = allPositionGenerator(problem).filter(positionChecker)
}