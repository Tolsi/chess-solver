package ru.tolsi.chess.solvers.dfs.withcache.bloomfilter

import com.google.common.hash.{Funnel, PrimitiveSink}
import ru.tolsi.chess.ChessPiecePosition

class ChessStateFunnel extends Funnel[Set[ChessPiecePosition]] {
  override def funnel(from: Set[ChessPiecePosition], into: PrimitiveSink): Unit = {
    from.foreach { pos =>
      into.putInt(pos.p.hashCode())
        .putInt(pos.piece.hashCode());
    }
  }
}
