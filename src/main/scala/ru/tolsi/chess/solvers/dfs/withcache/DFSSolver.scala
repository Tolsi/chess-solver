package ru.tolsi.chess.solvers.dfs.withcache

import com.google.common.hash.BloomFilter
import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.UniquePositionsFilter
import ru.tolsi.chess.solvers.dfs.withcache.bloomfilter.ChessStateFunnel


class DFSSolver extends ChessProblemSolver
  with UniquePositionsFilter {
  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = {
    val seenState = BloomFilter.create[Set[ChessPiecePosition]](new ChessStateFunnel, 150000000, 0.01)

    def solve(currentPieces: Vector[ChessPiece],
              problem: ChessProblem,
              currentState: ChessBoardState): Iterator[Set[ChessPiecePosition]] = {
      if (currentPieces.isEmpty) {
        Iterator(currentState.pieces)
      } else {
        for {
          position <- currentPieces.head.generateNextSafePositions(currentState)
          newPieces = currentState.pieces + position
          if !seenState.mightContain(newPieces)
          state = new ChessBoardState(newPieces, currentState.getSafePointsAfterPutPiece(position))
          solution <- {
            seenState.put(newPieces)
            solve(currentPieces.tail, problem, state)
          }
        } yield solution
      }
    }
    filterUniqueResults(solve(problem.pieces.sorted.toVector, problem, new ChessBoardState(problem)))
  }
}
