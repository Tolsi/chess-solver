package ru.tolsi.chess.solvers.dfs.withcache

import com.google.common.hash.BloomFilter
import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.UniquePositionsFilter
import ru.tolsi.chess.solvers.dfs.withcache.bloomfilter.ChessStateFunnel

import scala.concurrent.forkjoin.{ForkJoinPool, RecursiveTask}

class ParallelDFSSolverWithRotations extends ChessProblemSolver
  with WithBoardStateRotations
  with UniquePositionsFilter {
  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = {
    val parallelism = Runtime.getRuntime.availableProcessors() + 1
    val fjPool = new ForkJoinPool(parallelism)
    val fjTask = new FJ(problem.pieces.sorted.toVector, problem, new ChessBoardState(problem))
    fjPool.execute(fjTask)
    filterUniqueResults(fjTask.join())
  }

  private class FJ(currentPieces: Vector[ChessPiece],
                       problem: ChessProblem,
                       currentState: ChessBoardState,
                       seenState: BloomFilter[Set[ChessPiecePosition]] = BloomFilter.create[Set[ChessPiecePosition]](new ChessStateFunnel, 150000000, 0.01)) extends RecursiveTask[Iterator[Set[ChessPiecePosition]]] {
    override def compute(): Iterator[Set[ChessPiecePosition]] = {

      if (currentPieces.isEmpty) {
        val rotatedSolutions = getAllRotatedBoardStates(currentState.pieces, problem)
        rotatedSolutions.iterator
      } else {
        val fjTasks = for {
          position <- currentPieces.head.generateNextSafePositions(currentState).toSeq
          newPieces = currentState.pieces + position
          if seenState.synchronized(!seenState(newPieces))
          newSafePoints = currentState.getSafePointsAfterPutPiece(position)
          if currentPieces.tail.size <= newSafePoints.size
          state = new ChessBoardState(newPieces, newSafePoints)
        } yield {
          val rotatedSteps = getAllRotatedBoardStates(newPieces, problem)
          seenState.synchronized(rotatedSteps.foreach(seenState.put))
          new FJ(currentPieces.tail, problem, state, seenState)
        }
        fjTasks.foreach(_.fork())
        fjTasks.flatMap(_.join()).iterator
      }
    }
  }
}

