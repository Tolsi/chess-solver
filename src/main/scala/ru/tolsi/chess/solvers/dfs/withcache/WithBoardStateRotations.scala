package ru.tolsi.chess.solvers.dfs.withcache

import ru.tolsi.chess.{Point, ChessProblem, ChessPiecePosition}

trait WithBoardStateRotations {
  private[solvers] def getAllRotatedBoardStates(state: Set[ChessPiecePosition], problem: ChessProblem): Set[Set[ChessPiecePosition]] = {
    val mirorred = Set(
      state,
      state.map(pos => {
        pos.copy(p = Point(pos.p.x, problem.height - pos.p.y - 1))
      }),
      state.map(pos => {
        pos.copy(p = Point(problem.width - pos.p.x - 1, problem.height - pos.p.y - 1))
      }),
      state.map(pos => {
        pos.copy(p = Point(problem.width - pos.p.x - 1, pos.p.y))
      }))
    if (problem.width != problem.height) {
      mirorred
    } else {
      val rotated = Set(state.map(pos => pos.copy(p = Point(pos.p.y, pos.p.x))),
        state.map(pos => {
          pos.copy(p = Point(problem.height - pos.p.y - 1, pos.p.x))
        }),
        state.map(pos => {
          pos.copy(p = Point(problem.height - pos.p.y - 1, problem.width - pos.p.x - 1))
        }),
        state.map(pos => {
          pos.copy(p = Point(pos.p.y, problem.width - pos.p.x - 1))
        })
      )
      mirorred ++ rotated
    }
  }
}
