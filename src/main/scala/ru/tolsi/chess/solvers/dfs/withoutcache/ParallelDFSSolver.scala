package ru.tolsi.chess.solvers.dfs.withoutcache

import ru.tolsi.chess._
import ru.tolsi.chess.solvers.dfs.UniquePositionsFilter

import scala.concurrent.forkjoin.{ForkJoinPool, RecursiveTask}

class ParallelDFSSolver extends ChessProblemSolver
  with UniquePositionsFilter {

  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = {
    val parallelism = Runtime.getRuntime.availableProcessors() + 1
    val fjPool = new ForkJoinPool(parallelism)
    val fjTask = new FJ(problem.pieces.sorted.toVector, problem, new ChessBoardState(problem))
    fjPool.execute(fjTask)
    filterUniqueResults(fjTask.join())
  }

  private class FJ(currentPieces: Vector[ChessPiece],
                   problem: ChessProblem,
                   currentState: ChessBoardState) extends RecursiveTask[Iterator[Set[ChessPiecePosition]]] {
    override def compute(): Iterator[Set[ChessPiecePosition]] = {
      if (currentPieces.isEmpty) {
        Iterator(currentState.pieces)
      } else {
        val fjTasks = for {
          position <- currentPieces.head.generateNextSafePositions(currentState).toSeq
          newPieces = currentState.pieces + position
          newSafePoints = currentState.getSafePointsAfterPutPiece(position)
          if currentPieces.tail.size <= newSafePoints.size
          state = new ChessBoardState(newPieces, newSafePoints)
        } yield {
          new FJ(currentPieces.tail, problem, state)
        }
        fjTasks.foreach(_.fork())
        fjTasks.flatMap(_.join()).iterator
      }
    }
  }
}

