package ru.tolsi.chess.solvers.dfs

import ru.tolsi.chess.{ChessProblemSolver, ChessPiecePosition}

trait UniquePositionsFilter extends ChessProblemSolver {
  protected def filterUniqueResults(iterator: Iterator[Set[ChessPiecePosition]]) = iterator.toSet.iterator
}