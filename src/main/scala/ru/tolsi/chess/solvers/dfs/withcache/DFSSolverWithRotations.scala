package ru.tolsi.chess.solvers.dfs.withcache

import com.google.common.hash.BloomFilter
import ru.tolsi.chess.solvers.dfs.UniquePositionsFilter
import ru.tolsi.chess.solvers.dfs.withcache.bloomfilter.ChessStateFunnel
import ru.tolsi.chess.{ChessPiece, ChessPiecePosition, ChessProblem, _}

class DFSSolverWithRotations extends ChessProblemSolver
  with WithBoardStateRotations
  with UniquePositionsFilter {
  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = {
    val seenSteps = BloomFilter.create[Set[ChessPiecePosition]](new ChessStateFunnel, 150000000, 0.01)

    def solve(currentPieces: Vector[ChessPiece],
              problem: ChessProblem,
              currentState: ChessBoardState): Iterator[Set[ChessPiecePosition]] = {
      if (currentPieces.isEmpty) {
        val rotatedSolutions = getAllRotatedBoardStates(currentState.pieces, problem)
        rotatedSolutions.toIterator
      } else {
        for {
          position <- currentPieces.head.generateNextSafePositions(currentState)
          newPieces = currentState.pieces + position
          if !seenSteps.mightContain(newPieces)
          newSafePoints = currentState.getSafePointsAfterPutPiece(position)
          if currentPieces.tail.size <= newSafePoints.size
          state = new ChessBoardState(newPieces, newSafePoints)
          solution <- {
            getAllRotatedBoardStates(newPieces, problem).foreach(seenSteps.put)
            solve(currentPieces.tail, problem, state)
          }
        } yield solution
      }
    }

    filterUniqueResults(solve(problem.pieces.sorted.toVector, problem, new ChessBoardState(problem)))
  }
}
