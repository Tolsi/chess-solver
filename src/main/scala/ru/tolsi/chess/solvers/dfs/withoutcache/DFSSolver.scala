package ru.tolsi.chess.solvers.dfs.withoutcache

import ru.tolsi.chess.solvers.dfs.UniquePositionsFilter
import ru.tolsi.chess.{ChessPiece, ChessPiecePosition, ChessProblem, _}

class DFSSolver extends ChessProblemSolver
  with UniquePositionsFilter {
  override def apply(problem: ChessProblem): Iterator[Set[ChessPiecePosition]] = {

    def solve(currentPieces: Vector[ChessPiece],
              problem: ChessProblem,
              currentState: ChessBoardState): Iterator[Set[ChessPiecePosition]] = {
      if (currentPieces.isEmpty) {
        Iterator(currentState.pieces)
      } else {
        for {
          position <- currentPieces.head.generateNextSafePositions(currentState)
          newPieces = currentState.pieces + position
          newSafePoints = currentState.getSafePointsAfterPutPiece(position)
          if currentPieces.tail.size <= newSafePoints.size
          state = new ChessBoardState(newPieces, newSafePoints)
          solution <- solve(currentPieces.tail, problem, state)
        } yield solution
      }
    }

    filterUniqueResults(solve(problem.pieces.sorted.toVector, problem, new ChessBoardState(problem)))
  }
}
