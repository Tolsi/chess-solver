package ru.tolsi.chess.solvers.dfs.withcache

import ru.tolsi.chess.ChessPiecePosition

// WARNING: it makes not unique results on problems with large solutions number. But it fast and solve fast small problems
class WeakDFSSolverWithRotations extends DFSSolverWithRotations {
  override protected def filterUniqueResults(iterator: Iterator[Set[ChessPiecePosition]]): Iterator[Set[ChessPiecePosition]] = iterator
}
