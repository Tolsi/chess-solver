package ru.tolsi.chess.solvers.dfs.withcache

import ru.tolsi.chess.FastChessProblemSolverSpec

class ParallelDFSSolverSpec extends FastChessProblemSolverSpec {
  override val solver = new ParallelDFSSolverWithRotations
}
