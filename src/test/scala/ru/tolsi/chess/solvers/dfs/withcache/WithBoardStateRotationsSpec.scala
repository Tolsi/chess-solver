package ru.tolsi.chess.solvers.dfs.withcache

import org.scalatest.{Matchers, WordSpec}
import ru.tolsi.chess._

class WithBoardStateRotationsSpec extends WordSpec with Matchers {

  class FakeWithBoardStateRotations extends WithBoardStateRotations

  "DFSSolverWithRotations.allRotatedBoardState" should {
    "rotate square odd board state 1" in {
      val problem = ChessProblem(5, 5, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(King, Point(3, 3)))

      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(4)

      result should contain(Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(3, 3)),
        ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 3))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(3, 3)),
        ChessPiecePosition(King, Point(0, 4)),
        ChessPiecePosition(King, Point(1, 3))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(3, 3)),
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(King, Point(4, 4))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(4, 0)),
        ChessPiecePosition(King, Point(3, 3)),
        ChessPiecePosition(King, Point(1, 3))))
    }

    "rotate square odd board state 2" in {
      val problem = ChessProblem(5, 5, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(Rook, Point(1, 2)))

      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(8)

      result should contain(Set(
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(Rook, Point(2, 1))))

      result should contain(Set(
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(Rook, Point(2, 3))))

      result should contain(Set(
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(Rook, Point(3, 2))))

      result should contain(Set(
        ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(Rook, Point(2, 1))))

      result should contain(Set(
        ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(Rook, Point(1, 2))))

      result should contain(Set(
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(Rook, Point(1, 2))))


      result should contain(Set(
        ChessPiecePosition(King, Point(3, 3)),
        ChessPiecePosition(Rook, Point(2, 3))))
    }

    "rotate square even board state" in {
      val problem = ChessProblem(4, 4, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(King, Point(3, 3)))
      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(4)

      result should contain(Set(ChessPiecePosition(King, Point(1, 1)),
        ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(King, Point(3, 1)),
        ChessPiecePosition(King, Point(3, 3))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 0)),
        ChessPiecePosition(King, Point(1, 2)),
        ChessPiecePosition(King, Point(0, 3)),
        ChessPiecePosition(King, Point(3, 2)),
        ChessPiecePosition(King, Point(3, 0))))

      result should contain(Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(0, 2)),
        ChessPiecePosition(King, Point(2, 2)),
        ChessPiecePosition(King, Point(2, 0)),
        ChessPiecePosition(King, Point(3, 3))))

      result should contain(Set(ChessPiecePosition(King, Point(0, 3)),
        ChessPiecePosition(King, Point(2, 3)),
        ChessPiecePosition(King, Point(0, 1)),
        ChessPiecePosition(King, Point(2, 1)),
        ChessPiecePosition(King, Point(3, 0))))
    }

    "rotate not square even not symmetrical board state" in {
      val problem = ChessProblem(2, 4, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(Rook, Point(1, 3)))
      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(4)

      result should contain(Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(Rook, Point(1, 3))))

      result should contain(Set(ChessPiecePosition(King, Point(0, 3)),
        ChessPiecePosition(Rook, Point(1, 0))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 3)),
        ChessPiecePosition(Rook, Point(0, 0))))

      result should contain(Set(ChessPiecePosition(King, Point(1, 0)),
        ChessPiecePosition(Rook, Point(0, 3))))
    }

    "rotate not square odd not symmetrical board state" in {
      val problem = ChessProblem(1, 3, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(Rook, Point(0, 2)))
      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(2)

      result should contain(Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(Rook, Point(0, 2))))

      result should contain(Set(ChessPiecePosition(King, Point(0, 2)),
        ChessPiecePosition(Rook, Point(0, 0))))
    }

    "rotate square board state with one unique position" in {
      val problem = ChessProblem(2, 2, Seq.empty)
      val state = Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 0)),
        ChessPiecePosition(King, Point(0, 1)),
        ChessPiecePosition(King, Point(1, 1)))
      val result = new FakeWithBoardStateRotations().getAllRotatedBoardStates(state, problem)

      result.size should be(1)

      result should contain(Set(ChessPiecePosition(King, Point(0, 0)),
        ChessPiecePosition(King, Point(1, 0)),
        ChessPiecePosition(King, Point(0, 1)),
        ChessPiecePosition(King, Point(1, 1))))
    }
  }
}
