package ru.tolsi.chess.solvers.dfs.withcache

import ru.tolsi.chess._

class DFSSolverWithRotationsSpec extends FastChessProblemSolverSpec {
  override val solver = new DFSSolverWithRotations()
}
