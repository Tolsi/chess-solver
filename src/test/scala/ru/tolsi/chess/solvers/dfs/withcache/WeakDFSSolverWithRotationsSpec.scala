package ru.tolsi.chess.solvers.dfs.withcache

import ru.tolsi.chess._

class WeakDFSSolverWithRotationsSpec extends ChessProblemSolverSpec {
  override val solver = new WeakDFSSolverWithRotations()
}
