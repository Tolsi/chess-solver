package ru.tolsi.chess.solvers.dfs.withoutcache

import ru.tolsi.chess._

class DFSSolverSpec extends FastChessProblemSolverSpec {
  override val solver = new DFSSolver()
}
