package ru.tolsi.chess.solvers.dfs.withoutcache

import ru.tolsi.chess.FastChessProblemSolverSpec

class ParallelDFSSolverSpec extends FastChessProblemSolverSpec {
  override val solver = new ParallelDFSSolver
}
