package ru.tolsi.chess.solvers.bruteforce

import ru.tolsi.chess.{ChessProblemSolverSpec, SafePositionChecker}

class BruteForceSolverSpec extends ChessProblemSolverSpec {
  override val solver = new BruteForceSolver(new SafePositionChecker)
}
